Set-ExecutionPolicy Unrestricted

Write-Host "----------------------------------------"
Write-Host "Rechnername: "
& hostname
Write-Host ""
Write-Host "Aktueller Benutzer:"
& whoami
Write-Host ""
Write-Host "Windows Version:"
[Environment]::OSVersion
Write-Host ""
Write-Host "Release: " (Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion").ReleaseId
Write-Host "----------------------------------------"
Write-Host ""

& mkdir C:\temp

iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

#Privacy Settings
$url = "https://gitlab.com/GriepMarco/vagrant-windows10-installscripts/-/raw/master/InstallScripts/win-privacy.bat"
$output = "C:\temp\privacy.bat"
Invoke-WebRequest -Uri $url -OutFile $output
& cmd.exe /c 'C:\temp\privacy.bat'

$output = "C:\temp\runner.ps1"

#Default Applications
$url = "https://gitlab.com/GriepMarco/vagrant-windows10-installscripts/-/raw/master/InstallScripts/base-apps.ps1"
Invoke-WebRequest -Uri $url -OutFile $output
& $output

#Install Software Development Apps
$url = "https://gitlab.com/GriepMarco/vagrant-windows10-installscripts/-/raw/master/InstallScripts/20200122-mg-dev.ps1"
Invoke-WebRequest -Uri $url -OutFile $output
& $output

#Delete Shortcuts
$url = "https://gitlab.com/GriepMarco/vagrant-windows10-installscripts/-/raw/master/InstallScripts/delete-shortcuts.ps1"
Invoke-WebRequest -Uri $url -OutFile $output
& $output

$url = "https://gitlab.com/GriepMarco/vagrant-windows10-installscripts/-/raw/master/InstallScripts/german-keyboard.ps1"
Invoke-WebRequest -Uri $url -OutFile $output
& $output

#Install Guest Additions for Virtual Box (Use in Virtual Box only) - Necessary for bigger Resolution
$url = "https://gitlab.com/GriepMarco/vagrant-windows10-installscripts/-/raw/master/InstallScripts/virtualbox-guest-additions.ps1"
Invoke-WebRequest -Uri $url -OutFile $output
& $output