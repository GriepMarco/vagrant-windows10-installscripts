# Vagrant-Windows10-InstallScripts

This Vagrantfile will bring up an Windows 10 Virtual Machine with Vagrant. After installing vagrant will inject an powershell Script that installs Chocolatey Package Manager. Use the Scripts under "InstallScripts" to install more Applications. I use this Repo to bring up an development Machine with all necessary apps and SDKs like Python, Dotnet, Visual Studio Community and many more. The PowerShell Script will disable all the telemetry data.

## How to use this?

At first: Download and install Virtualbox or Hyper-V. Then install Vagrant on your Machine. Make an fork of this Repo to Customize the PowerShells for you needs.
The Script in "scripts" will be injected into the Virtual Machine on provisioning and run other scripts from this Repo. Comment out what you dont need or Add functionality you need.  
