choco install dotnet3.5 -y
choco install dotnet4.5 -y
choco install dotnet4.7 -y
choco install putty -y
choco install powershell -y
choco install irfanview -y
choco install 7zip -y
choco install foxitreader -y
choco install winscp -y
choco install googlechrome -y
    
$chromePath = "${Env:ProgramFiles(x86)}\Google\Chrome\Application\" 
$chromeApp = "chrome.exe"
$chromeCommandArgs = "--make-default-browser"
& "$chromePath$chromeApp" $chromeCommandArgs
