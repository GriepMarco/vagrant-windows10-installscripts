@ECHO OFF
REM /// #Rekonfiguriert Win10 Datenschutzeinstellungen
REM /// #Datum: 2016-02-05
REM /// #Author: Griep Marco

echo.
echo "==============================================="
echo " Griep Marco - Disable Win10 Privacy Settings"
echo "==============================================="
echo.
echo "Changing Registry Values..."
echo.
reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\DataCollection" /v AllowTelemetry /t REG_DWORD /d 1 /f
reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Policies\DataCollection" /v AllowTelemetry /t REG_DWORD /d 1 /f
reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows Defender" /v DisableAntiSpyware /t REG_DWORD /d 1 /f
reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\OneDrive" /v DisableFileSyncNGSC /t REG_DWORD /d 1 /f

echo.
echo "Trying to stop dmwappushservice"
sc stop dmwappushservice
goto WAITFORSTOP_DMWAPP

:WAITFORSTOP_DMWAPP
echo "Waiting...."
timeout 5 > NUL
echo.
echo "Check if service is not running anymore"
echo.
sc query dmwappushservice | Find /i "RUNNING"
IF ERRORLEVEL 1 GOTO PART_2
IF ERRORLEVEL 0 GOTO WAITFORSTOP_DMWAPP

:PART_2
echo.
echo "Successfully stopped dmwappushserivce"
echo "Disabling dmwappushservice..."
sc config dmwappushservice start= disabled

echo.
echo "Trying to stop DiagTrack"
sc stop DiagTrack
goto WAITFORSTOP_DiagTrack

:WAITFORSTOP_DiagTrack
echo "Waiting...."
timeout 5 > NUL
echo.
echo "Check if service is not running anymore"
echo.
sc query DiagTrack | Find /i "RUNNING"
IF ERRORLEVEL 1 GOTO PART_3
IF ERRORLEVEL 0 GOTO WAITFORSTOP_DiagTrack

:PART_3
echo.
echo "Successfully stopped DiagTrack"
echo "Disabling DiagTrack..."
sc config DiagTrack start= disabled

echo.
echo "Changing Scheduled Tasks..."
schtasks /Change /TN "\Microsoft\Windows\Application Experience\Microsoft Compatibility Appraiser" /DISABLE
schtasks /Change /TN "\Microsoft\Windows\Customer Experience Improvement Program\Consolidator" /DISABLE
schtasks /Change /TN "\Microsoft\Windows\Customer Experience Improvement Program\KernelCeipTask" /DISABLE
schtasks /Change /TN "\Microsoft\Windows\Application Experience\ProgramDataUpdater" /DISABLE
schtasks /Change /TN "\Microsoft\Windows\Customer Experience Improvement Program\UsbCeip" /DISABLE

echo.
echo "Script Finished..."
