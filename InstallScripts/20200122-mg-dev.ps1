choco install mysql.workbench -y
choco install python -y
#choco install openjdk -y
#choco install jdk8 -y
choco install android-sdk -y
#choco install androidstudio -y
choco install git -y
choco install dotnetcore-sdk -y
choco install vscode -y
#choco install visualstudio2019community --package-parameters "--allWorkloads --includeRecommended --includeOptional --passive --locale de-DE" -y
choco install docker -y
choco install steam -y
choco install github-desktop -y
